#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
:AUTHOR: Jonas Scherer
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: jonas.scherer@uni-bielefeld.de
:SINCE: Mon May 02 14:38
:VERSION: 0.1
:COMMENT: Outsourced based on Martin Müller (see "process_gpx.py")
"""

import os


def prepend_line(file_name, line):
    """
    This function prepends a new line at the beginning of a file. It can be used
    e.g. to prepend "sep=," at csv files, so that excel opens them correctly by default.

    Parameters
    ----------
    file_name : str
        String indicating path and filename of the file to be edited
    line : str
        String of line to be prepended to the file

    """
    # define name of temporary dummy file
    dummy_file = file_name + ".bak"
    # open original file in read mode and dummy file in write mode
    with open(file_name, "r") as read_obj, open(dummy_file, "w") as write_obj:
        # Write given line to the dummy file
        write_obj.write(line + "\n")
        # Read lines from original file one by one and append to the dummy
        for line in read_obj:
            write_obj.write(line)
    # remove original file
    os.remove(file_name)
    # Rename dummy file as the original file
    os.rename(dummy_file, file_name)
