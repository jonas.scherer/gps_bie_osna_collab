"""
:AUTHOR: Martin M. Müller
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: martin.mueller@uni-bielefeld.de
:SINCE: Fri Jun 11 13:51:12 2021
:VERSION: 0.1

This script allows to plot tracetories of gps data (gpx or pos) on a mpa that is automatically downloaded from openstreetmap.
"""
from tkinter import Tk
from tkinter.filedialog import askopenfilenames
import gps_utilities as gps_utils

savepath = r"D:\PhD\git\gps_analysis\Results\trajectories"

root = Tk()
root.withdraw()
root.attributes("-topmost", 1)  # bring window to front
track_paths = askopenfilenames(parent=root, title="select track files",)
root.attributes("-topmost", 0)  # send window to back
gps_utils.plot_gps_trajs(track_paths, savepath=savepath, num_ellipses=10)
