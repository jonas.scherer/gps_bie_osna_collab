"""
:AUTHOR: Jonas Scherer
:ORGANIZATION: Department of Neurobiology, Bielefeld University
:CONTACT: jonas.scherer@uni-bielefeld.de
:SINCE: Mon May 02 13:07
:VERSION: 0.1
:COMMENT: Outsourced based on Martin Müller (see "process_gpx.py")

This module contains functions that are preferably non-specific and as genrally applicable as possible.
"""
import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
import os
import requests
import shutil
import time
from gpx_converter import Converter
import pygeodesy as gd
import data_analysis.gps_helpers as gps_helps
from PIL import Image
from scipy.signal import find_peaks
from matplotlib import cm
from matplotlib.patches import Ellipse
from scipy.stats import t as t


def save_gpx_as_csv(filepaths):
    """
    Load a list of gpx files and save them as csv

    Parameters
    ----------
    filepaths : list
        List of strings, containing filepaths and files to gpx data

    Returns
    -------
    csv_paths : list
        list of strings containing csv paths

    """
    csv_paths = []
    for in_name in filepaths:
        # create new filename
        out_name = in_name[:-3] + "csv"
        # use gpx_converter lib to to do conversion
        Converter(input_file=in_name).gpx_to_csv(output_file=out_name)
        # add excel metadata
        gps_helps.prepend_line(out_name, "sep=,")
        # add new filepath to list of outputs
        csv_paths.append(out_name)
        # print progress
        print(f"saved track under: {out_name}")
    return csv_paths


def convert_geo_to_ecef(df):
    """
    Convert geodetic coordinates to earth centred earth fixed coordinates.

    Parameters
    ----------
    df : pandas DataFrame
        Dataframe containing latitude, longitude, altitude data.

    Returns
    -------
    df : pandas DataFrame
        altered DataFrame

    """
    df["ecef"] = df.apply(
        lambda row: gd.EcefKarney(gd.Ellipsoids.WGS84).forward(
            latlonh=row["latitude"], lon=row["longitude"], height=row["altitude"]
        ),
        axis=1,
    )
    return df


def convert_ecef_to_ltp(df):
    """
    Convert earth centred earth fixed to local tangent plane coordinates

    Parameters
    ----------
    df : pandas DataFrame
        Dataframe containing ecef data in columns named longitude, latitude and altitude

    Returns
    -------
    df : pandas DataFrame
        altered Dataframe

    """
    local_cart = gd.LocalCartesian(df.latitude[0], df.longitude[0], df.altitude[0])
    df["ltp"] = df.apply(lambda row: local_cart.forward(latlonh=row.ecef), axis=1)
    return df


def extract_xyz(df, colname="ltp"):
    """
    Extract xyz coordinates as individual columns

    Parameters
    ----------
    df : pandas DataFrame
        Dataframe containing gpx data with columns longitude, latitude and altitude
    colname : string, optional
        name for appended columns. "_x", "_y", "_z" are added automatically. The default is "ltp".

    Returns
    -------
    df : pandas DataFrame
        altered Dataframe

    """
    df.reindex(columns=df.columns.to_list().append(["x", "y", "z"]))
    for i in range(len(df)):
        df.loc[i, "x"] = df.loc[i, colname].x
        df.loc[i, "y"] = df.loc[i, colname].y
        df.loc[i, "z"] = df.loc[i, colname].z
    return df


def pos_to_dataframe(file):
    """
    Converts .pos data to dataframe with timestamp, latitude, longitude and height (alike .gpx data)

    Parameters
    ----------
    file : string
        path to .pos file

    Returns
    -------
    df : pandas dataframe
        dataframe containing global coordinates

    """
    df = pd.read_table(file, sep="\s+", header=9, parse_dates={"Timestamp": [0, 1]})
    df = df.rename(
        columns={
            "Timestamp": "time",
            "longitude(deg)": "longitude",
            "latitude(deg)": "latitude",
            "height(m)": "altitude",
        }
    )
    return df


def llh_to_dataframe(file):
    """
    Converts .llh data to dataframe with timestamp, latitude, longitude and height (alike .gpx data)

    Parameters
    ----------
    file : string
        path to .llh file

    Returns
    -------
    df : pandas dataframe
        dataframe containing global coordinates

    """
    df = pd.read_table(file, sep="\s+", header=None, parse_dates={"Timestamp": [0, 1]})
    df.columns = [
        "time",
        "latitude",
        "longitude",
        "altitude",
        "Q",  # Q=1:fix,2:float,3:sbas,4:dgps,5:single,6:ppp
        "n_satellites",
        "sdn_in_m",
        "sde_in_m",
        "sdu_in_m",
        "sdne_in_m",
        "sdeu_in_m",
        "sdun_in_m",
        "age_in_s",
        "ratio",
    ]
    return df


def deg2num(lat_deg, lon_deg, zoom):
    """
    Conversion from global coordinates to open street map slippy map tile number
    see for details:
    https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames

    Parameters
    ----------
    lat_deg : float, global coordinates
        latitude degree
    lon_deg : float, global coordinates
        longitude degree
    zoom : int
        zoom factor of slippy map

    Returns
    -------
    xtile : numeric
        slippy map x tile number
    ytile : numeric
        slippy map y tile number

    """
    # conversion from Slippy map tiles
    # see for details:
    # https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    lat_rad = math.radians(lat_deg)
    n = 2.0 ** zoom
    xtile = int((lon_deg + 180.0) / 360.0 * n)
    ytile = int(
        (1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi)
        / 2.0
        * n
    )
    return (xtile, ytile)


def num2deg(xtile, ytile, zoom):
    """
    Conversion from open street map slippy map tile number to global coordinates
    see for details:
    https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    
    Parameters
    ----------
    xtile : numeric
        number of xtile
    ytile : numeric
        number of ytile
    zoom : int
        zoom factor of slippy map

    Returns
    -------
    lat_deg : float, global coordinates
    lon_deg : float, global coordinates

    """
    # conversion from Slippy map tiles
    # see for details:
    # https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    n = 2.0 ** zoom
    lon_deg = xtile / n * 360.0 - 180.0
    lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
    lat_deg = math.degrees(lat_rad)
    return (lat_deg, lon_deg)


def rescale_value(old_min, old_max, new_min, new_max, old_value):
    """
    rescales values from one min-max range to a new one
    """
    old_range = old_max - old_min
    new_range = new_max - new_min

    new_value = ((old_value - old_min) * new_range / old_range) + new_min
    return new_value


def get_pixel_coordinates(image, wmin, wmax, hmin, hmax, pos, invert_y=True):
    """
    Projects trajectory points onto image and derives pixel values

    Parameters
    ----------
    image : str
        path to image
    wmin : numeric
        width minimum
    wmax : numeric
        width maximum
    hmin : numeric
        horizontal minimum
    hmax : numeric
        horizontal maximum
    pos : array-like
        holding list of coordinates (x in index [0] and y in index [1])
    invert_y : boolean, optional
        inverts y-coordinates The default is True.

    Returns
    -------
    points : TYPE
        DESCRIPTION.

    """
    img = Image.open(image)
    points = []
    for pos in zip(pos[0], pos[1]):
        x = rescale_value(wmin, wmax, 0, img.size[0], pos[0])
        y = rescale_value(hmin, hmax, 0, img.size[1], pos[1])
        if invert_y:
            y = img.size[1] - y
        points.append((x, y))
    return points


def getImageCluster(
    lat_deg, lon_deg, delta_lat, delta_long, zoom, save_name="temp_image.png"
):
    """
    Downloads open street map images for a certain global coordinate area and puts
    them together in a Cluster PIL.Image

    Parameters
    ----------
    lat_deg : float global coordinates
        latitude minimum
    lon_deg : float global coordinates
        longitude minimum
    delta_lat : float global coordinates
        latitude size of area (max minus min)
    delta_long : float global coordinates
        longitude size of area (max minus min)
    zoom : int
        slippy map zoom factor (see for details:  https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames)
    save_name : str, optional
        path and filename of image that downloads are saved to. The default is "temp_image.png".

    Returns
    -------
    Cluster : PIL.Image
        clustered images that were donwloaded
    lat_min : float, global coordinates
        latitude min of created image
    lat_max : float, global coordinates
        latitude max of created image
    long_min : float, global coordinates
        longitude min of created image
    long_max : float, global coordinates
        longitude max of created image

    """
    smurl = r"http://tile.openstreetmap.org/{0}/{1}/{2}.png"
    xmin, ymax = deg2num(lat_deg, lon_deg, zoom)
    xmax, ymin = deg2num(lat_deg + delta_lat, lon_deg + delta_long, zoom)

    Cluster = Image.new(
        "RGB", ((xmax - xmin + 1) * 256 - 1, (ymax - ymin + 1) * 256 - 1)
    )
    for xtile in range(xmin, xmax + 1):
        for ytile in range(ymin, ymax + 1):
            try:
                imgurl = smurl.format(zoom, xtile, ytile)
                print("Opening: " + imgurl)

                header = {
                    "User-Agent": "Jonas Scherer, PhD student at Bielefeld University, scientific purpose download, contact me at jonas.scherer@uni-bielefeld.de"
                }
                time.sleep(
                    1
                )  # sending more requests than 1 per second is not allowed following the Tile Server Usage Guidlines of OpenStreetMap
                r = requests.get(imgurl, stream=True, headers=header)
                if r.status_code == 200:
                    with open(save_name, "wb") as f:
                        shutil.copyfileobj(r.raw, f)
                    print("Image sucessfully Downloaded: ", save_name)
                else:
                    print("Image Couldn't be retrieved")
                tile = Image.open(save_name)
                Cluster.paste(tile, box=((xtile - xmin) * 256, (ytile - ymin) * 255))
            except:
                print("Couldn't download image")
                tile = None
    Cluster.save(save_name)
    # get the lat and long limits of the tiles that were used
    [lat_max, long_min] = list(num2deg(xmin, ymin, zoom))
    [lat_min, long_max] = list(
        num2deg(xmax + 1, ymax + 1, zoom)
    )  # +1 because functions return most north-west (uppper-left) cordinate
    return Cluster, lat_min, lat_max, long_min, long_max


def create_traj_plot(
    img,
    traj_points,
    ax=plt.gca(),
    height=None,
    width=None,
    num_ellipses=10,
    xlims=None,
    ylims=None,
    title=None,
):
    """
    Takes image and plots trajectory over it.

    Parameters
    ----------
    img : PIL.Image
        map image. Use 
    traj_points : TYPE
        DESCRIPTION.
    ax : TYPE, optional
        DESCRIPTION. The default is plt.gca().
    xlims : TYPE, optional
        DESCRIPTION. The default is None.
    ylims : TYPE, optional
        DESCRIPTION. The default is None.

    Returns
    -------
    ax : TYPE
        DESCRIPTION.

    """
    # xlims and ylims are only used as axes labels
    img = Image.open(img)
    ax.imshow(img)
    ax.grid()
    # plot traj
    ax.plot(
        [i[0] for i in traj_points], [i[1] for i in traj_points], linewidth=4, color="k"
    )
    # plot start and stop
    ax.scatter(traj_points[0][0], traj_points[0][1], color="k", marker=".", s=200)
    ax.scatter(traj_points[-1][0], traj_points[-1][1], color="k", marker=".", s=200)
    if not ((type(height) == type(None)) | (type(width) == type(None))):
        ellipses_pos = np.round(
            np.linspace(0, len(traj_points) - 1, num_ellipses)
        ).astype(int)
        for pos in ellipses_pos:
            # add error ellipses
            ell = Ellipse(
                xy=(traj_points[pos][0], traj_points[pos][1]),
                width=width[pos],
                height=height[pos],
                angle=0,
                color="black",
            )
            ax.add_patch(ell)
    # add labels
    ax.set_xlabel("Longitude")
    ax.set_ylabel("Latitude")
    ax.set_title(title)
    plt.tick_params(
        axis="both",  # changes apply to the x-axis
        which="both",  # both major and minor ticks are affected
        top=False,  # ticks along the top edge are off
        right=False,
    )  # labels along the bottom edge are off
    if (type(xlims) != None) & (type(ylims) != None):
        xlabs = [str(np.round(a, 3)) + "°E" for a in xlims]
        ylabs = [str(np.round(a, 3)) + "°N" for a in ylims]
        ax.set_xticks(ax.get_xlim(), labels=xlabs)
        ax.set_yticks(ax.get_ylim(), labels=ylabs)
    return ax


def plot_gps_trajs(track_paths, zoom=18, num_ellipses=10, savepath=None, title=None):
    """
    Takes trajectories, downloads respective map data from open street map api
    and plots trajectories onto the map.

    Parameters
    ----------
    track_paths : list
        list of .pos or .gpx files containing trajectory data
    zoom : int, optional
        zoom factor of open street map slippy map, >16 recommended, max is 19
        (see for details: https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames)
    """
    if type(track_paths) == str:
        track_paths = [track_paths]
    original_savepath = savepath
    for path in track_paths:
        # a savepath is needed to save the downloaded open street map pictures (are deleted later)
        if type(savepath) == type(None):
            savepath = os.path.splitext(path)[0]
        else:
            savepath = os.path.join(
                savepath, os.path.splitext(os.path.basename(path))[0]
            )
        if path[-4:] == ".gpx":
            df = Converter(input_file=path).gpx_to_dataframe()
        elif path[-4:] == ".pos":
            df = pos_to_dataframe(path)
        elif (path[-4:] == ".LLH") or (path[-4:] == ".llh"):
            df = llh_to_dataframe(path)
        fig_map, ax_map = plt.subplots()

        [lat_max, lat_min] = [max(df.latitude), min(df.latitude)]
        [long_max, long_min] = [max(df.longitude), min(df.longitude)]
        [lat_delta, long_delta] = [abs(lat_max - lat_min), abs(long_max - long_min)]
        Cluster, lat_min, lat_max, long_min, long_max = getImageCluster(
            lat_min - 0.1 * lat_delta,
            long_min - 0.1 * long_delta,
            lat_delta * 1.1,
            long_delta * 1.1,
            zoom=zoom,
            save_name=savepath + r".png",
        )
        img_points = get_pixel_coordinates(
            image=savepath + r".png",
            wmin=long_min,
            wmax=long_max,
            hmin=lat_min,
            hmax=lat_max,
            pos=np.array([df.longitude, df.latitude]),
        )
        sdnorth = None
        sdeast = None
        if "sdn(m)" in df.keys():
            sdnorth = df["sdn(m)"]
            sdeast = df["sde(m)"]
        create_traj_plot(
            img=savepath + r".png",
            traj_points=img_points,
            height=sdnorth,
            width=sdeast,
            num_ellipses=10,
            ax=ax_map,
            xlims=[long_min, long_max],
            ylims=[lat_min, lat_max],
            title=title,
        )
        if original_savepath:
            plt.savefig(savepath + r".png")
            print(r"Figure saved as: " + savepath + r".png")
        else:
            os.remove(savepath + r".png")


def cart2pol(x, y):
    rho = np.sqrt(x ** 2 + y ** 2)
    phi = np.arctan2(y, x)
    return (rho, phi)


def calc_rotation(x, y, left_is_positive=True):
    """Calculates rotation of a vector from the origin to x,y to the basis vector of the x axis.

    Args:
        x (double): x-position
        y (double): y-position

    Returns:
        rot: Rotation
    """
    rot = cart2pol(np.diff(x), np.diff(y))[1]
    if not left_is_positive:
        rot = -rot
    abs_rot = np.copy(rot)
    rot[~np.isnan(rot)] = np.unwrap(rot[~np.isnan(rot)])
    rot = np.append(rot, np.nan)
    abs_rot = np.append(abs_rot, np.nan)
    return rot, abs_rot


def calc_rotational_velocity(rot):
    """Calculate rotational velocity.

    Args:
        rot (double): Rotation.

    Returns:
        rot_vel: Rotational velocity in degree per second.
    """
    rot_vel = np.diff(rot)
    rot_vel = np.append(rot_vel, np.nan)
    return rot_vel


def rotate_coords(x, y, angle, angle_type="deg"):
    """Rotates coordinates in 2D space around the z-axis.

    Args:
        x (np.array): X-coordinates
        y (np.array): Y-coordinates
        angle (np.array or float): Angle of rotation
        angle_type (str, optional): Indicates if the angle is stated in degrees or radians. Can be 'deg' or 'rad'. Defaults to 'deg'.

    Returns:
        (np.array,np.array): Rotated X- and Y-coordinates
    """
    if angle_type == "deg":
        angle = np.radians(angle)
    # x_rotated = (x * np.cos(angle)) + (y * np.sin(angle))
    # y_rotated = (x * -np.sin(angle)) + (y * np.cos(angle))
    x_rotated = (x * np.cos(angle)) - (y * np.sin(angle))
    y_rotated = (x * np.sin(angle)) + (y * np.cos(angle))
    return x_rotated, y_rotated


def correct_turns(
    df, turn_window, min_turn_angle=45, turning_threshold=0.2, min_prominence=0.5
):
    """
    Finds turns in trajectory and corrects for them by the trajectory after the turn to be 
    aligned with the trajectory before the turn. At the turn datapoints can be deleted (defined by turn_window).

    Parameters
    ----------
    df : pd.DataFrame
        DataFrame with columns x,y,rot and rot_vel
    turn_window : list
        Defines window around the turn that will be deleted. 
        [start_of_window,end_of_window] start_of_window and end_of_window are the indices relative to the 
        turn that define the window. To contain the turn itself start_of_window must be negative integer 
        and end_of_window must be a positive integer.
    min_turn_angle : numeric, optional
        turns under this turning threshold get rejected. The default is 45.
    turning_threshold : numeric, optional
        turning velocity threshold. Turns under this threshold get rejected. The default is 0.2.
    min_prominence : numeric, optional
        value is fed forward to scipy.signal.find_peaks. turns under this threhsold get rejected. The default is 0.5.

    Returns
    -------
    df : pd.DataFrame
        new dataframe without turns.
    turns_new : array-like
        indices of where the turns are in the new dataframe.
    turns_old : array-like
        indices of where the turns where in the old dataframe.
    kept_turn_idxs : array-like
        indices of turns that were kept.
    rejected_turn_idxs : array-like
        indices of turns that were rejected.

    """
    # find turns
    peaks_high, _ = find_peaks(
        df["rot_vel"], height=turning_threshold, prominence=min_prominence
    )
    peaks_low, _ = find_peaks(
        -df["rot_vel"], height=turning_threshold, prominence=min_prominence
    )
    turns = np.sort(np.append(peaks_high, peaks_low))
    turns_old = np.copy(turns)
    turns_new = np.array([])
    kept_turn_idxs = []
    rejected_turn_idxs = []

    curr_turn_idx = 0
    turns_exist = False
    if len(turns) > 0:
        turns_exist = True
    while turns_exist:
        turn = turns[0]
        print(turn)
        # determine turn window indices
        turn_start = turn + turn_window[0]
        if turn_start < 0:
            turn_start = 0
        turn_end = turn + turn_window[1]
        if turn_end > len(df) - 1:
            turn_end = len(df) - 1
        # check if any next peaks are within turn_range -> if so extend the turn range
        for a in np.arange(1, len(turns)):
            if len(np.where(turns[a:] + turn_window[0] - 1 < turn_end)[0]) > 0:
                turn_end = turns[a] + turn_window[1]
                if turn_end > len(df) - 1:
                    turn_end = len(df) - 1
            else:
                break
        # calculate rotation angle of the turn
        before = df.loc[:, "rot"].iloc[turn_start]
        after = df.loc[:, "rot"].iloc[turn_end]
        angle = after - before
        correct_flag = True
        if abs((angle * 180 / np.pi)) < min_turn_angle:
            correct_flag = False
        n_turns_included = len(turns) - len(turns[[i >= turn_end for i in turns]])
        turns = turns[[i >= turn_end for i in turns]]
        if correct_flag:
            # choose all data points to be rotated
            xy = df.loc[:, ["x", "y"]].iloc[turn_end:]

            # move to be rotated data points to origin
            xy0 = df.loc[:, ["x", "y"]].iloc[turn_start]
            xy = xy - xy.iloc[0]

            # rotate data points
            x_rot, y_rot = rotate_coords(xy.x, xy.y, angle, angle_type="rad")
            # handle remained turns and turn range
            turn_range = np.arange(turn_start, turn_end)
            df.drop(df.index[turn_range], inplace=True)
            # move data points back
            df.loc[:, "x"].iloc[turn_start:] = x_rot + xy0.x
            df.loc[:, "y"].iloc[turn_start:] = y_rot + xy0.y
            turns -= len(turn_range)
        turns = turns[turns <= len(df)]
        turns_new = np.append(turns_new, turn_start)
        if correct_flag:
            kept_turn_idxs += [curr_turn_idx]
        else:
            rejected_turn_idxs += [curr_turn_idx]
        curr_turn_idx += n_turns_included
        if len(turns) == 0:
            turns_exist = False
    turns_new = turns_new.astype(int)
    return df, turns_new, turns_old, kept_turn_idxs, rejected_turn_idxs


def calc_speed(x, y, time=1):
    data = np.array((x, y))
    dist = np.diff(data, axis=1)
    speed = np.sqrt(np.sum(np.power(dist, 2), axis=0)) / time
    speed = np.append(speed, 0)
    return pd.Series(speed)


def process_gps_trajectory_with_turns(
    filename,
    savefile=None,
    smooth_win_size=5,
    speed_threshold=0.1,
    dist_threshold=5,
    turn_window=[-5, 10],
    min_turn_angle=45,
    turning_velocity_threshold=0.25,
    min_prominence=0.2,
    plot_flag=False,
):
    """
    Processes gps trajectories with turn to not contain turns after processing. It also plots figures of the turn processing

    Parameters
    ----------
    filename : str
        file containing the gps data.
    savefile : str, optional
        where to save the processed df. The processed df is not saved if None. The default is None.
    smooth_win_size : numeric, optional
        window size for gaussian smoothing of trajectory. The default is 5.
    speed_threshold : numeric, optional
        before thresh has been reached and after it fell below thresh, trajectory is not resepcted. The default is 0.1.
    dist_threshold : numeric, optional
        same as with speed_threshold but with distance from start position. The default is 5.
    turn_window : list, optional
        fed forward to correct_turns. The default is [-5, 10].
    min_turn_angle : numeric, optional
        fed forward to correct_turns. The default is 45.
    turning_velocity_threshold : numeric, optional
        fed forward to correct_turns. The default is 0.25.
    min_prominence : numeric, optional
        fed forward to correct_turns. The default is 0.2.
    plot_flag : boolean, optional
        whether or not to plot the process of turn compensation. The default is False.

    Returns
    -------
    None. But the 

    """
    path = filename
    if path[-4:] == ".gpx":
        df = Converter(input_file=path).gpx_to_dataframe()
    elif path[-4:] == ".pos":
        df = pos_to_dataframe(path)
    if plot_flag:
        # get open street map image of trajectory
        plot_gps_trajs(path, title=path)
    # add ecef tuple column (centred on earth centroid)
    df = convert_geo_to_ecef(df)
    # add ltp tuple column (centred on start of track, z is up, y is north)
    df = convert_ecef_to_ltp(df)
    # create individual ltp x y z columns
    df = extract_xyz(df)
    # drop tuple columns before saving
    df.drop(columns=["ecef", "ltp"], axis=1, inplace=True)
    # smooth a little bit
    df.loc[:, ["x", "y", "z"]] = (
        df.loc[:, ["x", "y", "z"]]
        .rolling(smooth_win_size, win_type="gaussian", center=True)
        .mean(std=2)
    )

    # calculate features
    df["rot"], df["abs_rot"] = calc_rotation(df.x, df.y, left_is_positive=False)
    df["rot_vel"] = calc_rotational_velocity(df.rot)
    # drop rows that contain nans due to smoothing and velocity calculations
    df.dropna(subset=["rot_vel"], inplace=True)
    df["v"] = calc_speed(df.x, df.y)
    # remove start and end where speed is under threshold
    speed_start, speed_end = (
        np.where(df.v > speed_threshold)[0][0],
        np.where(df.v > speed_threshold)[0][-1],
    )
    df = df.iloc[speed_start:speed_end, :]

    # correct turns
    turn_window = turn_window
    df_old = df.copy()
    df, turns_new, turns_old, kept_turn_idxs, rejected_turn_idxs = correct_turns(
        df=df,
        turn_window=turn_window,
        min_turn_angle=min_turn_angle,
        turning_threshold=turning_velocity_threshold,
        min_prominence=min_prominence,
    )
    # smooth trajectory a little more
    df.loc[:, ["x", "y"]] = (
        df.loc[:, ["x", "y"]]
        .rolling(smooth_win_size, win_type="gaussian", center=True)
        .mean(std=3)
    )
    df.dropna(inplace=True)
    keep = [i <= len(df) - 1 for i in turns_new]
    turns_new = turns_new[keep]
    turn_idxs = [
        np.sort(kept_turn_idxs + rejected_turn_idxs)[i]
        for i in np.arange(len(keep))[keep]
    ]
    kept_turn_idxs = [i for i in turn_idxs if i in kept_turn_idxs]
    rejected_turn_idxs = [i for i in turn_idxs if i in rejected_turn_idxs]
    if (
        len(df) > 0
    ):  # it can happen that there are no datapoints left if the recording was really bad
        # move trail with start to origin and rotate
        df.loc[:, ["x", "y"]] = df.loc[:, ["x", "y"]] - df.loc[:, ["x", "y"]].iloc[0]
        x_rot, y_rot = rotate_coords(
            df.loc[:, "x"], df.loc[:, "y"], df.loc[:, "rot"].iloc[0], angle_type="rad",
        )
        df.loc[:, "x"] = x_rot
        df.loc[:, "y"] = y_rot
        # reset index
        df = df.reset_index()
        # calculate features
        df["rot"], df["abs_rot"] = calc_rotation(df.x, df.y, left_is_positive=False)
        df["rot_vel"] = calc_rotational_velocity(df["rot"])
        df["v"] = calc_speed(df.x, df.y)
        df["dist"] = np.cumsum(df.v) - df.v.iloc[0]
        # cut first meters because there is a lot of noise happening
        df["valid"] = df.dist > dist_threshold

        if plot_flag:
            # make turn correction figure
            colors = cm.rainbow(np.linspace(0, 1, len(turns_old)))
            fig, ax = plt.subplots(2, 2)
            ax[0, 0].plot(df_old.x, df_old.y)
            ax[0, 0].scatter(
                df_old.loc[:, "x"].iloc[turns_old],
                df_old.loc[:, "y"].iloc[turns_old],
                marker="o",
                color=colors,
            )
            ax[0, 1].plot(np.arange(len(df_old.rot_vel)), df_old.rot_vel)
            ax[0, 1].scatter(
                turns_old, df_old.loc[:, "rot_vel"].iloc[turns_old], color=colors,
            )
            colors = cm.rainbow(np.linspace(0, 1, len(turns_old)))
            if len(turns_new) > 0:
                colors = colors[np.sort(kept_turn_idxs + rejected_turn_idxs)]
            else:
                colors = []
            ax[1, 0].plot(df.x, df.y)
            ax[1, 0].plot(
                df.loc[~df.valid, "x"],
                df.loc[~df.valid, "y"],
                color="red",
                linewidth=2,
            )
            ax[1, 0].scatter(
                df.loc[:, "x"].iloc[turns_new],
                df.loc[:, "y"].iloc[turns_new],
                marker="o",
                color=colors,
            )
            cross_out = [
                list(np.sort(kept_turn_idxs + rejected_turn_idxs)).index(i)
                for i in rejected_turn_idxs
            ]
            ax[1, 0].scatter(
                df.loc[:, "x"].iloc[turns_new[cross_out]],
                df.loc[:, "y"].iloc[turns_new[cross_out]],
                marker="x",
                color="black",
            )
            ax[1, 1].plot(np.arange(len(df.rot_vel)), df.rot_vel)
            ax[1, 1].plot(
                np.arange(len(df.loc[~df.valid, "rot_vel"])),
                df.loc[~df.valid, "rot_vel"],
                color="red",
                linewidth=2,
            )
            ax[1, 1].scatter(
                turns_new, df.loc[:, "rot_vel"].iloc[turns_new], color=colors
            )
            ax[0, 0].set_aspect("equal")
            ax[0, 0].set(
                xlabel="x [m]", ylabel="y [m]", title="vor der Drehpunkt-Korrektur"
            )
            ax[0, 1].set(
                xlabel="Zeit [Datenpunkte]",
                ylabel="Rotationsgeschwindigkeit [°/Datenpunkt]",
                title="vor der Drehpunkt-Korrektur",
            )
            ax[1, 0].set_aspect("equal")
            ax[1, 0].set(
                xlabel="x [m]", ylabel="y [m]", title="nach der Drehpunkt-Korrektur"
            )
            ax[1, 1].set(
                xlabel="Zeit [Datenpunkte]",
                ylabel="Rotationsgeschwindigkeit [°/Datenpunkt]",
                title="nach der Drehpunkt-Korrektur",
            )
            plt.suptitle(path)
        # correct for problem in correct_turns() (some data points are double)
        df = df[df.v > 0.001]
        df = df[df.valid]
        if type(savefile) != type(None):
            # save gpx as .csv
            csv_file = savefile + ".csv"
            df.to_csv(csv_file, index=False)
            # add excel header again
            gps_helps.prepend_line(csv_file, "sep=,")
            print(f"processed file saved as: {savefile}")


def calc_ci(x, conf=0.95):
    x = np.array(x)
    s = np.nanstd(x)
    n = len(x)
    dof = n - 1
    t_crit = np.abs(t.ppf((1 - conf) / 2, dof))
    ci = s * t_crit / np.sqrt(n)

    return ci
